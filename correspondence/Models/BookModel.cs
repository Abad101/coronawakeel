﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace correspondence.Models
{
    public class BookModel : DB
    {
        //return Log id after adding
        public int AddLog(int log_type, string UserName_ID, string Platform, string IP, string Type)
        {
            DateTime ODate = DateTime.Now;
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                SqlCommand command = connect.CreateCommand();
                command.CommandText = "insert into LogFile (log_type, UserName_ID, Platform, IP, Type) OUTPUT inserted.id  values " +
                    "(@log_type, @UserName_ID, @Platform, @IP, @Type)";

                command.Parameters.AddWithValue("log_type", log_type);
                command.Parameters.AddWithValue("UserName_ID", UserName_ID);
                command.Parameters.AddWithValue("Platform", Platform);
                command.Parameters.AddWithValue("IP", IP);
                command.Parameters.AddWithValue("Type", Type);
                // get the log id
                Int32 log_id = (Int32)command.ExecuteScalar();

                return log_id;
            }
            catch (Exception ex)
            {
                throw (ex);
                return 0;
            }
            finally
            {
                connect.Dispose();
            }
        }
        //method AddBook end

        //return book id after adding
        public string AddBook(string Book_Num, DateTime Book_Date, string Subject, string BookType, string Destination, string Status, string Book_Response, string Duration, string Wazeer_Note, string Secret, string attach_id, DateTime Resp_Date , string Ward_Num, DateTime? Ward_Date, string user_id)
        {
            DateTime ODate = DateTime.Now;
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                if (attach_id == "0")
                    attach_id = null;
                if (Duration == "0")
                    Status = "1";
                SqlCommand command = connect.CreateCommand();
                command.CommandText = "insert into Books (Book_Num, Book_Date, Subject, Destination, Status, Book_Response, Duration, Wazeer_Note, Secret, Attachment, Response_Date, Ward_num, Ward_Date, Added_By, Book_Type) OUTPUT inserted.id  values " +
                    "(@Book_Num, @Book_Date, @Subject, @Destination, @Status, @Book_Response, @Duration, @Wazeer_Note, @Secret, @Attachment, @Response_Date, @Ward_num, @Ward_Date, @Added_By, @Book_Type)";

                command.Parameters.AddWithValue("Book_Num", Book_Num);
                command.Parameters.AddWithValue("Book_Date", Book_Date);
                command.Parameters.AddWithValue("Subject", Subject);
                command.Parameters.AddWithValue("Destination", Destination);
                command.Parameters.AddWithValue("Status", Status);
                command.Parameters.AddWithValue("Book_Response", Book_Response);
                command.Parameters.AddWithValue("Duration", Duration);
                command.Parameters.AddWithValue("Wazeer_Note", Wazeer_Note ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Secret", Secret);
                command.Parameters.AddWithValue("Attachment", attach_id ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Response_Date", Resp_Date);
                command.Parameters.AddWithValue("Ward_num", Ward_Num ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Ward_Date", Ward_Date ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Added_By", user_id);
                command.Parameters.AddWithValue("Book_Type", BookType);

                // get the book id
                Int32 book_id = (Int32)command.ExecuteScalar();
                return book_id.ToString();
            }
            catch
            {
                return "0";
            }
            finally
            {
                connect.Dispose();
            }
        }
        //method AddBook end 

        
        //method to add Book Location
        public int AddLocation(List<string> Book_Location, string Book_id , string BookType)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                foreach(string BookL in Book_Location)
                {
                    if((BookL != "") && (BookL != null))
                    {
                        SqlCommand command = connect.CreateCommand();
                        command.CommandText = "insert into Book_Location (name, Book_id, Book_Type) values " +
                            "(@name, @Book_id, @Book_Type)";

                        command.Parameters.AddWithValue("name", BookL);
                        command.Parameters.AddWithValue("Book_id", Book_id);
                        command.Parameters.AddWithValue("Book_Type", BookType);
                        command.ExecuteNonQuery();
                    }
                }
                return 1;

            }
            catch
            {
                return 0;
            }
            finally
            {
                connect.Dispose();
            }
        }
        //end method

        //method to add CopyTO 
        public int AddCopyTo(List<string> CopyTo, string Book_id)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                foreach (string Cop in CopyTo)
                {
                    if ((Cop != "") && (Cop != null))
                    {
                        SqlCommand command = connect.CreateCommand();
                        command.CommandText = "insert into CopyTo (name, Book_id) values " +
                            "(@name, @Book_id)";

                        command.Parameters.AddWithValue("name", Cop);
                        command.Parameters.AddWithValue("Book_id", Book_id);
                        command.ExecuteNonQuery();
                    }
                }
                return 1;

            }
            catch
            {
                return 0;
            }
            finally
            {
                connect.Dispose();
            }
        }
        //end method
        public string AddOrUdateFile(byte[] file, string FileName, string Content_typ, int size, string Book_id, int id)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                string attach_id = "";
                if (id == 0)
                {
                    SqlCommand command = connect.CreateCommand();
                    command.CommandText = "insert into Attachment (Attach, Name, content_type, size, Book_id) OUTPUT inserted.id  values (@Attach, @Name, @content_type, @size, @Book_id)";

                    command.Parameters.AddWithValue("Attach", file ?? System.Data.SqlTypes.SqlBinary.Null);
                    command.Parameters.AddWithValue("Name", FileName ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("content_type", Content_typ ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("size", size);
                    command.Parameters.AddWithValue("Book_id", Book_id);

                    // get the Attachmet id
                    attach_id = Convert.ToString((Int32)command.ExecuteScalar());
                }
                else
                {
                    SqlCommand command = connect.CreateCommand();
                    command.CommandText = "UPDATE Attachment SET Name = @Name, Attach = @Attach, content_type = @content_type, size = size where id = @id";

                    command.Parameters.AddWithValue("Name", FileName ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("Attach", file ?? System.Data.SqlTypes.SqlBinary.Null);
                    command.Parameters.AddWithValue("content_type", Content_typ ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("size", size);
                    command.Parameters.AddWithValue("id", Book_id);
                    command.ExecuteNonQuery();
                }
                return attach_id;
            }
            catch(Exception ex)
            {
                throw ex;
                return "0";
            }
            finally
            {
                connect.Dispose();
            }
        }

        //print all Books  
        public List<ViewModels.Books> PrintBooks(string Book_num, string Subject, string Book_Date, string Status, string BookType, string User_Dept)
        {
            List<ViewModels.Books> Books = new List<ViewModels.Books>();
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                DateTime? B_Date;
                DateTime Result;
                int isNull_Book_Num, isNull_Subject, isNull_Book_Date, isNull_Status, isNull_BookType, isNull_Destination;
                isNull_Book_Num  = 0;
                isNull_Subject = 0;
                isNull_Book_Date = 0;
                isNull_Status = 0;
                isNull_BookType = 0;
                isNull_Destination = 0;

                if ((Book_num == "") || (Book_num == null))
                    isNull_Book_Num = 1;

                if (Subject == "")
                    isNull_Subject = 1;

                if (Status == "")
                    isNull_Status = 1;

                if (DateTime.TryParse(Book_Date, out Result) == false)
                {
                    isNull_Book_Date = 1;
                    B_Date = null;
                }
                else
                    B_Date = Result;

                if ((BookType == null) || (BookType == ""))
                    isNull_BookType = 1;

                if (User_Dept == "0")
                    isNull_Destination = 1;

                using (SqlCommand command = new SqlCommand("select DATEDIFF(DAY, sysdatetime(), Response_Date) AS DateDiff, * from Books where (@isNull_Book_Num = 1 Or Book_Num = @Book_Num) And (@isNull_Subject = 1 Or Subject like @Subject) "
                    + "And (@isNull_Book_Date = 1 Or Book_Date = @Book_Date) And (@isNull_Status = 1 Or Status = @Status) And (@isNull_BookType = 1 Or Book_Type = @Book_Type) And (@isNull_Destination = 1 Or Destination = @Destination Or Destination = 0) Order by DateDiff, Book_Num ASC", connect))
                {
                    command.Parameters.AddWithValue("isNull_Book_Num", isNull_Book_Num);
                    command.Parameters.AddWithValue("Book_Num", Book_num  ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("isNull_Subject", isNull_Subject);
                    command.Parameters.AddWithValue("Subject", "%" + Subject + "%" ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("isNull_Book_Date", isNull_Book_Date);
                    command.Parameters.AddWithValue("Book_Date", B_Date ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("isNull_Status", isNull_Status);
                    command.Parameters.AddWithValue("Status", Status ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("isNull_BookType", isNull_BookType);
                    command.Parameters.AddWithValue("Book_Type", BookType ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("isNull_Destination", isNull_Destination);
                    command.Parameters.AddWithValue("Destination", User_Dept ?? (object)DBNull.Value);
                    
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            DateTime Book_Date_Format, Resp_date;
                            string BDate, stat, p_id;
                            string WardNum, WardDate;
                            int? Durat,Attach_id;

                            if (!DBNull.Value.Equals(dr["Ward_num"]))
                                WardNum = dr["Ward_num"].ToString();
                            else
                                WardNum = "";

                            if (!DBNull.Value.Equals(dr["Ward_Date"]))
                            {
                                Book_Date_Format = (DateTime)dr["Ward_Date"];
                                WardDate = Book_Date_Format.Year + "/" + Book_Date_Format.Month + "/" + Book_Date_Format.Day;
                            }
                            else
                                WardDate = "";

                            if (!DBNull.Value.Equals(dr["Duration"]))
                                Durat = Convert.ToInt32(dr["Duration"]);
                            else
                                Durat = null;

                            if (!DBNull.Value.Equals(dr["Response_Date"]))
                                Resp_date = Convert.ToDateTime(dr["Response_Date"]);
                            else
                                Resp_date = DateTime.Now;

                            if (!DBNull.Value.Equals(dr["Status"]))
                            {
                                if (Convert.ToInt16(dr["Status"].ToString()) == 1)
                                    stat = "تم الرد";
                                else if(Convert.ToInt16(dr["Status"].ToString()) == 2)
                                    stat = "لم يتم الرد";
                                else
                                    stat = "----";
                            }
                            else
                                stat = "----";

                            if (!DBNull.Value.Equals(dr["Parent_id"]))
                                p_id = dr["Parent_id"].ToString();
                            else
                                p_id = null;

                            if (!DBNull.Value.Equals(dr["Book_Date"]))
                            {
                                Book_Date_Format = (DateTime)dr["Book_Date"];
                                BDate = Book_Date_Format.Year + "/" + Book_Date_Format.Month + "/" + Book_Date_Format.Day;
                            }
                            else
                                BDate = "";

                            if (!DBNull.Value.Equals(dr["Attachment"]))
                                Attach_id = Convert.ToInt32(dr["Attachment"]);
                            else
                                Attach_id = 0;


                            Books.Add(new ViewModels.Books
                            {
                                id = Convert.ToInt32(dr["id"].ToString()),
                                Book_Num = dr["Book_Num"].ToString(),
                                BookType = dr["Book_Type"].ToString(),
                                Ward_num = WardNum,
                                Ward_Date = WardDate,
                                Book_Date = BDate,
                                Subject = dr["Subject"].ToString(),
                                Destination = dr["Destination"].ToString(),
                                Status = stat,
                                Book_Response = dr["Book_Response"].ToString(),
                                Book_ResponseDate = Resp_date,
                                Duration = Durat,
                                Wazeer_Note = dr["Wazeer_Note"].ToString(),
                                Added_Date = Convert.ToDateTime(dr["Added_Date"]),
                                Attachment = Attach_id,
                                Parent_id = p_id
                            });
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Dispose();
            }
            return Books;
        }
        //print all books method end 


        //Get a book details for editing  
        public ViewModels.Books GetBook(string id)
        {
            ViewModels.Books Books = new ViewModels.Books();
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                using (SqlCommand command = new SqlCommand("select * from Books where id =@id order by id desc", connect))
                {
                    command.Parameters.AddWithValue("id", id);
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            int? Durat, attach_id, BookType;
                            string stat, BDate, RDate, WardNum, WardDate, p_id;
                            DateTime Book_Date_Format;

                            if (!DBNull.Value.Equals(dr["Ward_num"]))
                                WardNum = dr["Ward_num"].ToString();
                            else
                                WardNum = "";

                            if (!DBNull.Value.Equals(dr["Ward_Date"]))
                            {
                                Book_Date_Format = (DateTime)dr["Ward_Date"];
                                WardDate = Book_Date_Format.Year + "/" + Book_Date_Format.Month + "/" + Book_Date_Format.Day;
                            }
                            else
                                WardDate = "";

                            if (!DBNull.Value.Equals(dr["Duration"]))
                                Durat = Convert.ToInt32(dr["Duration"]);
                            else
                                Durat = 5;

                            if (!DBNull.Value.Equals(dr["Book_Type"]))
                                BookType = Convert.ToInt16(dr["Book_Type"].ToString());
                            else
                                BookType = 0;

                            if (!DBNull.Value.Equals(dr["Status"]))
                                stat = dr["Status"].ToString();
                            else
                                stat = "0";

                            if (!DBNull.Value.Equals(dr["Parent_id"]))
                                p_id = dr["Parent_id"].ToString();
                            else
                                p_id = null;

                            if (!DBNull.Value.Equals(dr["Attachment"]))
                                attach_id = Convert.ToInt32(dr["Attachment"]);
                            else
                                attach_id = null;

                            if (!DBNull.Value.Equals(dr["Book_Date"]))
                            {
                                Book_Date_Format = (DateTime)dr["Book_Date"];
                                BDate = Book_Date_Format.Year + "/" + Book_Date_Format.Month + "/" + Book_Date_Format.Day;
                            }
                            else
                                BDate = "";

                            if (!DBNull.Value.Equals(dr["Response_Date"]))
                            {
                                Book_Date_Format = (DateTime)dr["Response_Date"];
                                RDate = Book_Date_Format.Year + "/" + Book_Date_Format.Month + "/" + Book_Date_Format.Day;
                            }
                            else
                                RDate = "";

                            Books.id = Convert.ToInt32(dr["id"].ToString());
                            Books.Book_Num = dr["Book_Num"].ToString();
                            Books.Book_Date = BDate;
                            Books.Ward_num = WardNum;
                            Books.Ward_Date = WardDate;
                            Books.BookType = BookType.ToString();
                            Books.Subject = dr["Subject"].ToString();
                            Books.Destination = dr["Destination"].ToString();
                            Books.Status = stat;
                            Books.Book_Response = dr["Book_Response"].ToString();
                            Books.Duration = Durat;
                            Books.Wazeer_Note = dr["Wazeer_Note"].ToString();
                            Books.Attachment = attach_id;
                            Books.Parent_id = p_id;
                            Books.Secret = Convert.ToInt16(dr["Secret"].ToString());
                            Books.Response_Date = RDate;
                            Books.Added_Date = Convert.ToDateTime(dr["Added_Date"].ToString());
                        }


                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Dispose();
            }
            return Books;
        }
        //Get book details for editing method end 

        //Get Book Locations method
        public List<ViewModels.Book_Location> GetBookLocations(string id)
        {
            SqlConnection connect = ConnectToDB();
            List<ViewModels.Book_Location> Book_Loc = new List<ViewModels.Book_Location>();
            try
            {
                connect.Open();
                
                using (SqlCommand command = new SqlCommand("select * from Book_Location where Book_id =@Book_id order by id ASC", connect))
                {
                    command.Parameters.AddWithValue("Book_id", id);
                    using (SqlDataReader dr2 = command.ExecuteReader())
                    {
                        int Count = 1;
                        while (dr2.Read())
                        {
                            string T_id;
                            string L_Text;

                            if (dr2["Book_Type"].ToString() == "1")
                            {
                                T_id = "WardLoc" + Count;
                                L_Text = "مصدر الكتاب" + Count;
                            }
                            else
                            {
                                T_id = "SaderLoc" + Count;
                                L_Text = "الجهة المرسل اليها" + Count;
                            }
                            
                            Book_Loc.Add(new ViewModels.Book_Location
                            {
                                id = Convert.ToInt32(dr2["id"].ToString()),
                                Name = dr2["name"].ToString(),
                                Book_id = dr2["Book_id"].ToString(),
                                Book_type = dr2["Book_Type"].ToString(),
                                Text_id = T_id,
                                Label_text = L_Text
                            });
                            Count += 1;

                        }
                        dr2.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Dispose();
            }
            return Book_Loc;
        }
        //End of GetBookLocations

        //Get CopyTo method
        public List<ViewModels.CopyTo> GetCopyTo(string id)
        {
            SqlConnection connect = ConnectToDB();
            List<ViewModels.CopyTo> CopyTo = new List<ViewModels.CopyTo>();
            try
            {
                connect.Open();

                using (SqlCommand command = new SqlCommand("select * from CopyTo where Book_id =@Book_id order by id ASC", connect))
                {
                    command.Parameters.AddWithValue("Book_id", id);
                    using (SqlDataReader dr2 = command.ExecuteReader())
                    {
                        int Count = 1;
                        while (dr2.Read())
                        {
                            string T_id;
                            string L_Text;
                            T_id = "CopyTo" + Count;
                            L_Text = "نسخة الى" + Count;

                            CopyTo.Add(new ViewModels.CopyTo
                            {
                                id = Convert.ToInt32(dr2["id"].ToString()),
                                Name = dr2["name"].ToString(),
                                Book_id = dr2["Book_id"].ToString(),
                                Text_id = T_id,
                                Label_text = L_Text
                            });
                            Count += 1;

                        }
                        dr2.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Dispose();
            }
            return CopyTo;
        }
        //End of GetCopyTo


        //Get GetAttachments method
        public List<ViewModels.Attachment> GetBookAttachments(string id)
        {
            SqlConnection connect = ConnectToDB();
            List<ViewModels.Attachment> Attachs = new List<ViewModels.Attachment>();
            try
            {
                connect.Open();

                using (SqlCommand command = new SqlCommand("select * from Attachment where Book_id =@Book_id order by id ASC", connect))
                {
                    command.Parameters.AddWithValue("Book_id", id);
                    using (SqlDataReader dr2 = command.ExecuteReader())
                    {
                        while (dr2.Read())
                        {

                            Attachs.Add(new ViewModels.Attachment
                            {
                                id = Convert.ToInt32(dr2["id"]),
                                Attach = (Byte[])dr2["Attach"],
                                Name = dr2["Name"].ToString(),
                                content_type = dr2["content_type"].ToString(),
                                size = dr2["size"].ToString()
                            });

                        }
                        dr2.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Dispose();
            }
            return Attachs;
        }
        //End of GetAttachments

        //Edit Book data
        public int EditBook(string id, string Book_Num, DateTime Book_Date, string Subject, string BookType, string Destination, string Status, string Book_Response, string Duration, string Wazeer_Note, string Secret, string attach_id, DateTime? Resp_Date, string Parent_id, string Ward_Num, DateTime? Ward_Date)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                if (Duration == "0")
                    Status = "1";
                connect.Open();
                SqlCommand command = connect.CreateCommand();
                command.CommandText = "UPDATE Books SET Book_Num = @Book_Num, Book_Date = @Book_Date, Subject = @Subject, Destination = @Destination, Status = @Status, " 
                   + "Book_Response = @Book_Response, Duration = @Duration, Wazeer_Note = @Wazeer_Note, Secret = @Secret, Attachment = @Attachment, Response_Date= @Response_Date, "
                   + "Parent_id = @Parent_id, Book_Type = @Book_Type , Ward_num = @Ward_num , Ward_Date = @Ward_Date where id = @id";

                command.Parameters.AddWithValue("Book_Num", Book_Num);
                command.Parameters.AddWithValue("Book_Date", Book_Date);
                command.Parameters.AddWithValue("Subject", Subject);
                command.Parameters.AddWithValue("Destination", Destination ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Status", Status ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Book_Response", Book_Response ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Duration", Duration);
                command.Parameters.AddWithValue("Wazeer_Note", Wazeer_Note ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Secret", Secret);
                command.Parameters.AddWithValue("Attachment", attach_id ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Response_Date", Resp_Date ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Parent_id", Parent_id ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Book_Type", BookType ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Ward_num", Ward_Num ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("Ward_Date", Ward_Date ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("id", id);
                command.ExecuteNonQuery();

                return 1;
            }
            catch 
            {
                return 0;
            }
            finally
            {
                connect.Dispose();
            }
        }
        //End EditBook method


        //Edit BookLocation data
        public int EditBookLocation(List<ViewModels.Book_Location> Old_BLocation , List<string> BLocation, string BookType, string Book_id)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                int OldCounter = Old_BLocation.Count;
                int NewCounter = BLocation.Count;
                int Count = 0;
                while(Count < OldCounter)
                {
                    if ((BLocation[Count] != "") && (BLocation[Count] != null))
                    {
                        SqlCommand command = connect.CreateCommand();
                        command.CommandText = "UPDATE Book_Location SET name = @name where id = @id";

                        command.Parameters.AddWithValue("name", BLocation[Count]);
                        command.Parameters.AddWithValue("id", Old_BLocation[Count].id);
                        command.ExecuteNonQuery();
                        
                    }
                    else
                    {
                        SqlCommand command = connect.CreateCommand();
                        command.CommandText = "DELETE From Book_Location Where id = @id ";
                        command.Parameters.AddWithValue("id", Old_BLocation[Count].id);
                        command.ExecuteNonQuery();
                    }
                    Count += 1;
                }

                while (Count < NewCounter)
                {
                    if ((BLocation[Count] != "") && (BLocation[Count] != null))
                    {
                        SqlCommand command = connect.CreateCommand();
                        command.CommandText = "insert into Book_Location (name, Book_id, Book_Type) values " +
                            "(@name, @Book_id, @Book_Type)";

                        command.Parameters.AddWithValue("name", BLocation[Count]);
                        command.Parameters.AddWithValue("Book_id", Book_id);
                        command.Parameters.AddWithValue("Book_Type", BookType);
                        command.ExecuteNonQuery();
                        
                    }
                    Count += 1;
                }


                return 1;
            }
            catch
            {
                return 0;
            }
            finally
            {
                connect.Dispose();
            }
        }
        //End EditBookLocation method

        //Edit CopyTo data
        public int EditCopyTo(List<ViewModels.CopyTo> Old_CopyTo, List<string> CopyTo, string Book_id)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                int OldCounter = Old_CopyTo.Count;
                int NewCounter = CopyTo.Count;
                int Count = 0;
                while (Count < OldCounter)
                {
                    if ((CopyTo[Count] != "") && (CopyTo[Count] != null))
                    {
                        SqlCommand command = connect.CreateCommand();
                        command.CommandText = "UPDATE CopyTo SET name = @name where id = @id";

                        command.Parameters.AddWithValue("name", CopyTo[Count]);
                        command.Parameters.AddWithValue("id", Old_CopyTo[Count].id);
                        command.ExecuteNonQuery();
                        
                    }
                    else
                    {
                        SqlCommand command = connect.CreateCommand();
                        command.CommandText = "DELETE From CopyTo Where id = @id ";
                        command.Parameters.AddWithValue("id", Old_CopyTo[Count].id);
                        command.ExecuteNonQuery();
                    }
                    Count += 1;

                }

                while (Count < NewCounter)
                {
                    if ((CopyTo[Count] != "") && (CopyTo[Count] != null))
                    {
                        SqlCommand command = connect.CreateCommand();
                        command.CommandText = "insert into CopyTo (name, Book_id) values " +
                            "(@name, @Book_id)";

                        command.Parameters.AddWithValue("name", CopyTo[Count]);
                        command.Parameters.AddWithValue("Book_id", Book_id);
                        command.ExecuteNonQuery();
                    }
                    Count += 1;

                }


                return 1;
            }
            catch
            {
                return 0;
            }
            finally
            {
                connect.Dispose();
            }
        }
        //End EditBookLocation method

        public int DeleteBook(string id)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                SqlCommand command = connect.CreateCommand();
                command.CommandText = "DELETE From Books Where id = @id ";
                command.Parameters.AddWithValue("id", id);
                command.ExecuteNonQuery();

                return 1;
            }
            catch 
            {
                return 0;
            }
            finally
            {
                connect.Dispose();
            }
        }

        //Delete all old BookLocations (This is used in EditingBook)
        public int DeleteBookLoc(string id)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                
                SqlCommand command = connect.CreateCommand();
                command.CommandText = "DELETE From Book_Location Where Book_id = @Book_id ";
                command.Parameters.AddWithValue("Book_id", id);
                command.ExecuteNonQuery();

                return 1;
            }
            catch
            {
                return 0;
            }
            finally
            {
                connect.Dispose();
            }
        }
        //End DeleteBookLoc method

        //Delete all old BookLocations (This is used in EditingBook)
        public int DeleteCopyTo(string id)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                
                SqlCommand command = connect.CreateCommand();
                command.CommandText = "DELETE From CopyTo Where Book_id = @Book_id ";
                command.Parameters.AddWithValue("Book_id", id);
                command.ExecuteNonQuery();

                return 1;
            }
            catch
            {
                return 0;
            }
            finally
            {
                connect.Dispose();
            }
        }
        //End DeleteBookLoc method

        public ViewModels.Attachment DownloadFile(string id)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                ViewModels.Attachment attach = new ViewModels.Attachment();
                SqlCommand command2 = new SqlCommand("select * from Attachment Where id = @id", connect);

                command2.Parameters.AddWithValue("id", id);

                SqlDataReader dr1 = null;
                dr1 = command2.ExecuteReader();
                if (dr1.HasRows)
                {
                    if (dr1.Read())
                    {
                        attach.Name = Convert.ToString(dr1["Name"]);
                        attach.content_type = dr1["content_type"].ToString();
                        attach.Attach = (Byte[])dr1["Attach"];
                        attach.size = dr1["size"].ToString();
                    }
                }
                return attach;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Dispose();
            }
        }

        public List<ViewModels.Books> PrintAllPaernt(string id)
        {
            List<ViewModels.Books> Books = new List<ViewModels.Books>();
            List<ViewModels.Books> NewBooks = new List<ViewModels.Books>();
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();

                using (SqlCommand command = new SqlCommand("with tbParent as" 
                  +  "( "
                  +  "select * from Books where id = @id "
                  +  "union all "
                  + "select Books.* from Books join tbParent on Books.id = tbParent.parent_id "
                  + ") "
                  + "SELECT * FROM  tbParent", connect))
                {
                    command.Parameters.AddWithValue("id", id);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            DateTime Book_Date_Format;
                            string BDate, stat;
                            int? Durat, p_id;

                            //if (!DBNull.Value.Equals(dr["Duration"]))
                            //    Durat = Convert.ToInt32(dr["Duration"]);
                            //else
                            //    Durat = null;

                            //if (!DBNull.Value.Equals(dr["Status"]))
                            //{
                            //    if (Convert.ToInt16(dr["Status"].ToString()) == 1)
                            //        stat = "تم الرد";
                            //    else if (Convert.ToInt16(dr["Status"].ToString()) == 2)
                            //        stat = "لم يتم الرد";
                            //    else
                            //        stat = "----";
                            //}
                            //else
                            //    stat = "----";

                            //if (!DBNull.Value.Equals(dr["Parent_id"]))
                            //    p_id = Convert.ToInt32(dr["Parent_id"]);
                            //else
                            //    p_id = null;

                            if (!DBNull.Value.Equals(dr["Book_Date"]))
                            {
                                Book_Date_Format = (DateTime)dr["Book_Date"];
                                BDate = Book_Date_Format.Year + "/" + Book_Date_Format.Month + "/" + Book_Date_Format.Day;
                            }
                            else
                                BDate = "";


                            Books.Add(new ViewModels.Books
                            {
                                id = Convert.ToInt32(dr["id"].ToString()),
                                Book_Num = dr["Book_Num"].ToString(),
                                Book_Date = BDate,
                                Subject = dr["Subject"].ToString()
                            });
                        }

                        if (Books.Count > 0)
                        {
                            int Count = Books.Count - 1;
                            while (Count >= 0)
                            {
                                NewBooks.Add(new ViewModels.Books
                                {
                                    id = Books[Count].id,
                                    Book_Num = Books[Count].Book_Num,
                                    Book_Date = Books[Count].Book_Date,
                                    Subject = Books[Count].Subject
                                });
                                Count -= 1;
                            }
                        }
                        dr.Close();
                    }

                }

                Books = new List<ViewModels.Books>();

                using (SqlCommand command = new SqlCommand("with tbsons as"
                  + "( "
                  + "select * from Books where id = @id "
                  + "union all "
                  + "select Books.* from Books join tbsons  on Books.Parent_id= tbsons.id "
                  + ") "
                  + "SELECT * FROM  tbsons", connect))
                {
                    command.Parameters.AddWithValue("id", id);

                    using (SqlDataReader dr1 = command.ExecuteReader())
                    {
                        while (dr1.Read())
                        {
                            DateTime Book_Date_Format;
                            string BDate, stat;
                            int? Durat, p_id;

                            //if (!DBNull.Value.Equals(dr["Duration"]))
                            //    Durat = Convert.ToInt32(dr["Duration"]);
                            //else
                            //    Durat = null;

                            //if (!DBNull.Value.Equals(dr["Status"]))
                            //{
                            //    if (Convert.ToInt16(dr["Status"].ToString()) == 1)
                            //        stat = "تم الرد";
                            //    else if (Convert.ToInt16(dr["Status"].ToString()) == 2)
                            //        stat = "لم يتم الرد";
                            //    else
                            //        stat = "----";
                            //}
                            //else
                            //    stat = "----";

                            //if (!DBNull.Value.Equals(dr["Parent_id"]))
                            //    p_id = Convert.ToInt32(dr["Parent_id"]);
                            //else
                            //    p_id = null;

                            if (!DBNull.Value.Equals(dr1["Book_Date"]))
                            {
                                Book_Date_Format = (DateTime)dr1["Book_Date"];
                                BDate = Book_Date_Format.Year + "/" + Book_Date_Format.Month + "/" + Book_Date_Format.Day;
                            }
                            else
                                BDate = "";


                            Books.Add(new ViewModels.Books
                            {
                                id = Convert.ToInt32(dr1["id"].ToString()),
                                Book_Num = dr1["Book_Num"].ToString(),
                                Book_Date = BDate,
                                Subject = dr1["Subject"].ToString()
                            });
                        }

                        dr1.Close();

                        if (Books.Count > 0)
                        {
                            int Count = 0;
                            while (Count < Books.Count)
                            {
                                if((Count == 0) && (NewBooks.Count > 0))
                                {
                                    //do nothing
                                }
                                else
                                    NewBooks.Add(new ViewModels.Books
                                    {
                                        id = Books[Count].id,
                                        Book_Num = Books[Count].Book_Num,
                                        Book_Date = Books[Count].Book_Date,
                                        Subject = Books[Count].Subject
                                    });

                                Count += 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Dispose();
            }
            return NewBooks;
        }
    }
}