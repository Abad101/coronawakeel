﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace correspondence.Models
{
    public class User_Department
    {
        public static Dictionary<int, String> Levels = new Dictionary<int, String>
        {
            {0, "الكل" },
            {1, "المراسم" },
            {2, "الشئون القانونية"},
            {3, "الشئون العامة"},
            {4, "مكتب رئيس الهيئة"},
            {5, "خدمة المواطن"},
            {6, "الشئون العسكرية"}
        };
    }
}