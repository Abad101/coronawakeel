﻿using System;
using System.Data.SqlClient;
using Ensues.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;

namespace correspondence.Models
{
    public class UserModel : DB
    {
        public ViewModels.UserVM Login(string username, string password, int platform = 1) // platform 1=web 2=mobile
        {
            ViewModels.UserVM user = null;
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                using (SqlCommand command1 = new SqlCommand("SELECT username,password,id,name,lastlogin,user_level,User_Department from Users where username=@username", connect))
                {
                    command1.Parameters.AddWithValue("username", username);
                    using (SqlDataReader dr = command1.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            var pa = new PasswordAlgorithm();
                            pa.SaltLength = 64;
                            pa.HashFunction = HashFunction.SHA512;
                            pa.HashIterations = 10000;
                            Debug.Write("pass" + dr["PASSWORD"].ToString());
                            if (pa.Compare(password, dr["PASSWORD"].ToString()))
                            {
                                user = new ViewModels.UserVM { name = dr["name"].ToString(), username = dr["username"].ToString(), id = Convert.ToInt32(dr["id"].ToString()), user_level = Convert.ToInt32(dr["user_level"].ToString()), lastlogin = dr["lastlogin"].ToString(), auth = true, success = true , user_Dept = Convert.ToInt16(dr["User_Department"].ToString()) };
                                dr.Close();
                                if (platform == 1)
                                {
                                    using (SqlCommand command2 = new SqlCommand("UPDATE USERS SET LASTLOGIN = @LASTLOGIN where username = @username", connect))
                                    {

                                        command2.Parameters.AddWithValue("LASTLOGIN", DateTime.Now.ToString());
                                        command2.Parameters.AddWithValue("username", username);
                                        command2.ExecuteNonQuery();
                                    }
                                }
                                else if (platform == 2)
                                {
                                    string token = Convert.ToBase64String(Guid.NewGuid().ToByteArray()) + Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                                    user.token = token;
                                    using (SqlCommand command2 = new SqlCommand("UPDATE USERS SET LASTLOGIN_MOBILE = @LASTLOGIN, token=@token where username = @username", connect))
                                    {

                                        command2.Parameters.AddWithValue("LASTLOGIN", DateTime.Now.ToString());
                                        command2.Parameters.AddWithValue("token", token);
                                        command2.Parameters.AddWithValue("username", username);
                                        command2.ExecuteNonQuery();
                                    }
                                }


                                return user;
                            }
                            else
                            {
                                return user;
                            }

                        }
                        else
                        {
                            return user;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        public bool isAuth(string username, string token) // platform 1=web 2=mobile
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                using (SqlCommand command1 = new SqlCommand("SELECT username,token from Users where username=@username and token=@token", connect))
                {
                    command1.Parameters.AddWithValue("username", username);
                    command1.Parameters.AddWithValue("token", token);
                    using (SqlDataReader dr = command1.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            return true;

                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }


        }




        public int Register(ViewModels.UserVM user)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                // Check if the user is not exisit
                using (SqlCommand command1 = new SqlCommand("SELECT Count(*) from Users where username= @username", connect))
                {
                    command1.Parameters.AddWithValue("username", user.username);

                    // if the user exisit
                    if (Convert.ToInt32(command1.ExecuteScalar()) > 0)
                    {
                        // 2 mean user exist
                        return 2;
                    }
                    else
                    {
                        // encrypt password
                        var pa = new PasswordAlgorithm();
                        pa.SaltLength = 64;
                        pa.HashFunction = HashFunction.SHA512;
                        pa.HashIterations = 10000;
                        string password = pa.Compute(user.password);

                        SqlCommand command2 = connect.CreateCommand();

                        command2.CommandText = "INSERT INTO USERS (USERNAME,PASSWORD,NAME,USER_LEVEL,User_Department) VALUES (@USERNAME,@PASSWORD,@NAME,@USER_LEVEL,@User_Department)";
                        command2.Parameters.AddWithValue("USERNAME", user.username);
                        command2.Parameters.AddWithValue("PASSWORD", password);
                        command2.Parameters.AddWithValue("NAME", user.name);
                        command2.Parameters.AddWithValue("USER_LEVEL", user.user_level);
                        command2.Parameters.AddWithValue("User_Department", user.user_Dept);
                        command2.ExecuteNonQuery();

                        return 1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
                return 0;
            }
        }









        public List<ViewModels.UserVM> get(int id = 0)
        {
            List<ViewModels.UserVM> users = new List<ViewModels.UserVM>();
            SqlConnection con = ConnectToDB();
            try
            {
                con.Open();
                string statment = "select id,username,name,lastlogin,user_level from users ";
                if (id != 0)
                    statment = "select id,username,name,lastlogin,user_level from users where id=@id";
                using (SqlCommand command = new SqlCommand(statment, con))
                {
                    if (id != 0)
                        command.Parameters.Add(new SqlParameter("id", id));
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ViewModels.UserVM user = new ViewModels.UserVM { name = dr["name"].ToString(), username = dr["username"].ToString(), id = Convert.ToInt32(dr["id"].ToString()), user_level = Convert.ToInt32(dr["user_level"].ToString()), lastlogin = dr["lastlogin"].ToString(), };
                            users.Add(user);
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Dispose();
            }

            return users;
        }



        public bool ChangePassword(string old_password, string password, string username)
        {
            SqlConnection connect = ConnectToDB();
            try
            {
                connect.Open();
                using (SqlCommand command1 = new SqlCommand("SELECT username,password from Users where username=@username", connect))
                {
                    command1.Parameters.AddWithValue("username", username);
                    using (SqlDataReader dr = command1.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            
                            var pa = new PasswordAlgorithm();
                            pa.SaltLength = 64;
                            pa.HashFunction = HashFunction.SHA512;
                            pa.HashIterations = 10000;
                            if (pa.Compare(old_password, dr["PASSWORD"].ToString()))
                            {
                                dr.Close();
                                command1.Dispose();
                                // change password here
                                // encrypt password   
                                string enc_password = pa.Compute(password);

                                SqlCommand command2 = connect.CreateCommand();

                                command2.CommandText = "UPDATE USERS SET PASSWORD=@PASSWORD WHERE USERNAME=@USERNAME";
                                command2.Parameters.AddWithValue("PASSWORD", enc_password);
                                command2.Parameters.AddWithValue("USERNAME", username);
                                command2.ExecuteNonQuery();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return false;
        }
    }
}