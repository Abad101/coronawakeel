﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace correspondence.Models
{
    public static class UserLevels
    {
        public static Dictionary<int, String> Levels = new Dictionary<int, String>
        {
            {1, "مدير نظام" },
            {2, "وزير"},
            {3, "إدخال كتب"},
            {4, "متابعة كتب"}
        };
    }
}