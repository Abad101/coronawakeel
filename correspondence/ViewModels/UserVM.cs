﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace correspondence.ViewModels
{
    public class UserVM
    {
        public string username { set; get; }
        public string password { set; get; }
        public string name { set; get; }
        public string token { set; get; }
        public int id { set; get; }
        public int user_level { set; get; }
        public int user_Dept { set; get; }
        public string lastlogin { set; get; }
        public string lastlogin_mobile { set; get; }
        public bool auth { set; get; }
        public bool success { set; get; }


    }
}