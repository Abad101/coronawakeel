﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace correspondence.ViewModels
{
    public class Books
    {
        public int id { set; get; }
        public string Book_Num { set; get; }
        public string Book_Date { set; get; }
        public string Ward_num { set; get; }
        public string Ward_Date { set; get; }
        public string Subject { set; get; }
        public string BookType { set; get; } // 1 9ader, 2 wared
        public string Destination { set; get; }
        public string Status { set; get; } // 1 answered , 2 not answer
        public string Book_Response { set; get; }
        public DateTime Book_ResponseDate { set; get; }
        public int? Duration { set; get; }
        public int? Attachment { set; get; }
        public string Wazeer_Note { set; get; }
        public int Added_By { set; get; }
        public DateTime Added_Date { set; get; }
        public string Parent_id { set; get; }
        public int Secret { set; get; }
        public string Response_Date { set; get; }
        public int Book_rem;
    }
}