﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace correspondence.ViewModels
{
    public class Attachment
    {
        public int id { set; get; }
        public byte[] Attach { set; get; }
        public string Name { set; get; }
        public string content_type { set; get; }
        public string size { set; get; }
        public string Book_id { set; get; }
    }
}