﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace correspondence.ViewModels
{
    public class CopyTo
    {
        public int id { set; get; }
        public string Name { set; get; }
        public string Book_id { set; get; }
        public string Text_id { set; get; }
        public string Label_text { set; get; }
    }
}