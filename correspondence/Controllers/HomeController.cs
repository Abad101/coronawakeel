﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace correspondence.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");
                
            }
            
            ViewBag.alert = TempData["alert"];
            ViewBag.error = TempData["error"];
            return RedirectToAction("NotAnsweredBook", "Book");
            
        }
    }
}