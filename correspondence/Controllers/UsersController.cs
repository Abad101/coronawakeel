﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace correspondence.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Login()
        {
            ViewBag.alert = TempData["alert"];
            ViewBag.error = TempData["error"];

            return View();
        }

        // GET: Users/Register
        public ActionResult Register()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            ViewBag.alert = TempData["alert"];
            ViewBag.error = TempData["error"];
            ViewBag.levels = Models.UserLevels.Levels;
            ViewBag.User_Department = Models.User_Department.Levels;
            return View();
        }
        // GET: Users/Shpw
        public ActionResult Show()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            var model = new Models.UserModel();
            ViewBag.alert = TempData["alert"];
            ViewBag.error = TempData["error"];
            ViewBag.users = model.get();
            ViewBag.levels = Models.UserLevels.Levels;
            return View();
        }


        public ActionResult Logining()
        {


            // get values from form
            string username = Request["username"].ToString();
            string password = Request["password"].ToString();

            var userModel = new Models.UserModel();
            ViewModels.UserVM user = userModel.Login(username, password);

            if (user!=null)
            {
                Session["auth_isLogin"] = true;
                Session["auth_level"] = user.user_level;
                Session["auth_Department"] = user.user_Dept;
                Session["auth_name"] = user.name;
                Session["auth_username"] = user.username;
                Session["auth_userid"] = user.id;
                TempData["alert"] = "مرحباً " + user.name + " ، اهلا بك في نظام المراسلات ";
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["error"] = "الرقم المدني أو كلمة المرور خاطأة";
                return RedirectToAction("Login", "Users");
            }

           
        }

        public ActionResult Logout()
        {
            Session.Clear();
            TempData["alert"] = "تم تسجيل الخروج";
            return RedirectToAction("Login", "Users");

        }

        public ActionResult Registering()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }

            // get values from form
            string username = Request["username"].ToString();
            string password = Request["password"].ToString();
            string comfirm_password = Request["comfirm_password"].ToString();
            string name = Request["name"].ToString();
            string user_level = Request["user_level"].ToString();
            string user_Depts = Request["user_Dept"].ToString();

            if (comfirm_password.CompareTo(password) != 0)
            {
                TempData["error"] = "كلمات المرور غير متطابقة";
                return RedirectToAction("Register");
            }
            var userModel = new Models.UserModel();
            int register = userModel.Register(new ViewModels.UserVM { username = username, password = password, name=name, user_level = Convert.ToInt32(user_level) , user_Dept = Convert.ToInt32(user_Depts) });

            // user exists
            if (register == 2)
            {
                TempData["error"] = "المستخدم موجود مسبقاً";
                return RedirectToAction("Register");
            }
            else if (register == 0)
            {
                TempData["error"] = "حدث خطأ أثناء اضافة المستخدم بقاعدة البيانات";
                return RedirectToAction("Register");
            }
            else
            {
                TempData["alert"] = "تم إضافة المستخدم بنجاح";
                return RedirectToAction("Index", "Home");
            }
           
        }



        public ActionResult ChangePassword()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            ViewBag.alert = TempData["alert"];
            ViewBag.error = TempData["error"];
            return View();
        }
        [HttpPost]
        public ActionResult ChangingPassword()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            string old_password = Request["old_password"].ToString();
            string password = Request["password"].ToString();
            string comfirm_password = Request["comfirm_password"].ToString();

            if (comfirm_password.CompareTo(password) != 0)
            {
                TempData["error"] = "كلمات المرور الجديدة غير متطابقة";
                return RedirectToAction("ChangePassword");
            }
            Models.UserModel model = new Models.UserModel();
            bool result = model.ChangePassword(old_password, password, (string)Session["auth_username"]);
            if (result)
            {
                TempData["alert"] = "تم تغيير كلمة المرور بنجاح";
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["alert"] = "كلمة المرور القديمة خاطأة";
                return RedirectToAction("ChangePassword");
            }
        }
    }
}