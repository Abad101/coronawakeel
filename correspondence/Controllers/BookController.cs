﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;


namespace correspondence.Controllers
{
    public class BookController : Controller
    {
        // GET: Book
        public ActionResult Index()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            
            return View();
        }

        [HttpGet]
        public ActionResult AddBook()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            string Dept = Session["auth_Department"].ToString();
            if ((Dept == "0") || (Dept == "4"))
                ViewBag.BookNum = "110000";
            else if (Dept == "1")
                ViewBag.BookNum = "113100";
            else if (Dept == "2")
                ViewBag.BookNum = "110100";
            else if (Dept == "3")
                ViewBag.BookNum = "112000";
            else if (Dept == "5")
                ViewBag.BookNum = "114000";
            else
                ViewBag.BookNum = "1110100";

            ViewBag.alert = TempData["alert"];
            ViewBag.error = TempData["error"];
            ViewBag.BookLoc = 1; //counter for book Location
            ViewBag.BookLoc2 = 1;
            ViewBag.CopyTo = 1;
            return View();
        }

        [HttpPost]
        public ActionResult AddingBook(HttpPostedFileBase Postfile)
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            try
            {
                string Book_Num = Request["Book_Num"].ToString();
                string SaderBook_Num = Request["Book_Num2"].ToString();
                string Book_Date = Request["Book_Date"].ToString();
                string Subject = Request["Subject"].ToString();
                string BookType = Request["BookType"].ToString();
                string Destination = Request["Destination"].ToString();
                //string Status = Request["Status"].ToString();
                string Duration = Request["Duration"].ToString();
                //string Book_Response = Request["Book_Response"].ToString();
                string Wazeer_Note = Request["Wazeer_Note"].ToString();
                string Secret = Request["Secret"].ToString();
                DateTime B_Date, W_Date;
                List<string> BookLocation = new List<string>();
                List<string> CopyToList = new List<string>();
                string WardBook_Num = "";
                string WardBook_Date;
                int WardFlag = 1;
                W_Date = DateTime.Now;

                if (BookType == "1")
                {
                    WardBook_Num = Request["BookWard_Num"].ToString();
                    WardBook_Date = Request["BookWard_Date"].ToString();
                    int Wardcounter = Convert.ToInt16(Request["WardLocCounter"].ToString());
                    if (DateTime.TryParse(WardBook_Date, out W_Date) == false)
                        WardFlag = 0;
                    else
                    {
                        int i = 1;
                        while (i <= Wardcounter)
                        {
                            string WardLoc = "WardLoc" + i;
                            BookLocation.Add(Request[WardLoc].ToString());
                            i = i + 1;
                        }
                    }
                }
                else
                {
                    int Sadercounter = Convert.ToInt16(Request["SaderLocCounter"].ToString());
                    int CopyCounter = Convert.ToInt16(Request["CopyCounter"].ToString());
                    int i = 1;
                    while (i <= Sadercounter)
                    {
                        string SaderLoc = "SaderLoc" + i;
                        BookLocation.Add(Request[SaderLoc].ToString());
                        i = i + 1;
                    }

                    i = 1;
                    while (i <= CopyCounter)
                    {
                        string CopyTo = "CopyTo" + i;
                        CopyToList.Add(Request[CopyTo].ToString());
                        i = i + 1;
                    }
                }


                if ((WardFlag == 0) || (DateTime.TryParse(Book_Date, out B_Date) == false) || ((BookType == "1") && (Book_Num.ToString().Count() == 0)) || ((BookType == "2") && (SaderBook_Num.ToString().Count() == 0)) || (Book_Date.ToString().Count() == 0) || (Subject.ToString().Count() == 0))
                {
                    TempData["error"] = "حدث خطأ ، لم يتم إضافة الكتاب ... الرجاء ادخال جميع البيانات المطلوبة";
                }
                else
                {
                    Models.BookModel Books = new Models.BookModel();
                    string Attach_id = null;
                    DateTime Resp_Date = DateTime.Now;
                    int durat = 1;
                    
                    while(durat <= Convert.ToInt16(Duration))
                    {
                        Resp_Date = Resp_Date.AddDays(1);
                        if((Resp_Date.DayOfWeek.ToString() != "Friday") && (Resp_Date.DayOfWeek.ToString() != "Saturday"))
                             durat += 1;
                    }
                    string user_id = Session["auth_userid"].ToString();
                    string book_id;
                    if (BookType == "1")
                    {

                        WardBook_Num = Request["ReadBook_Num2"].ToString() + Request["BookWard_Num"].ToString();
                        book_id = Books.AddBook(Book_Num, B_Date, Subject, BookType, Destination, "2", "", Duration, Wazeer_Note, Secret, Attach_id, Resp_Date, WardBook_Num, W_Date, user_id);
                    }
                    else
                    {
                         SaderBook_Num = Request["ReadBook_Num"].ToString() + Request["Book_Num2"].ToString();
                         book_id = Books.AddBook(SaderBook_Num, B_Date, Subject, BookType, Destination, "2", "", Duration, Wazeer_Note, Secret, Attach_id, Resp_Date, "", null, user_id);
                    }
                       

                    int Loc_id =Books.AddLocation(BookLocation, book_id, BookType);

                    if (BookType == "2")
                    {
                        int Copy_id = Books.AddCopyTo(CopyToList, book_id.ToString());
                    }

                    if ((Postfile != null) && (Postfile.ContentLength > 0) && (Convert.ToUInt64(book_id) > 0))
                    {
                        string filename = Path.GetFileName(Postfile.FileName);
                        string extention = Path.GetExtension(Postfile.FileName).ToLower();
                        String[] allowedExtensions = { ".gif", ".png", ".jpeg", ".jpg", ".doc", ".docx", ".pdf", ".fni", ".xls", ".xlt", ".xlm", ".ppt", ".pot", ".pps" };
                        if (allowedExtensions.Contains(extention))
                        {
                            using (Stream fs = Postfile.InputStream)
                            {
                                using (BinaryReader br = new BinaryReader(fs))
                                {

                                    string contenttype = Postfile.ContentType;
                                    int filesize = Postfile.ContentLength;
                                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    Attach_id = Books.AddOrUdateFile(bytes, filename, contenttype, filesize, book_id, 0);

                                }
                            }
                        }
                    }

                    if (Convert.ToUInt64(book_id) > 0)
                    {
                        TempData["alert"] = "تم إضافة الكتاب بنجاح";
                        int log_id = Books.AddLog(1, user_id, "Web", "", "");
                        return RedirectToAction("SearchBook");
                    }
                    else
                        TempData["alert"] = " حدث خطأ ، لم يتم إضافة الكتاب  ";
                }
                
            }
            catch
            {
                TempData["error"] = "حدث خطأ ، لم يتم إضافة الكتاب";
            }
            return RedirectToAction("AddBook");

        }

        public ActionResult SearchBook( string Book_num , string Subject , string Book_Date, int? page)
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            Models.BookModel Books = new Models.BookModel();
            string User_Dept = Session["auth_Department"].ToString();
            ViewBag.Books = Books.PrintBooks(Book_num, Subject, Book_Date, "", "", User_Dept).ToPagedList(page ?? 1, 20);
            return View(ViewBag.Books);
        }


        [HttpPost]
        public ActionResult SearchingBook()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            string Book_num = Request["Book_num"].ToString();
            string Subject = Request["Subject"].ToString();
            string Book_Date = Request["Book_Date"].ToString();
            return RedirectToAction("SearchBook", new {  Book_num, Subject, Book_Date });
        }

        [HttpPost]
        public ActionResult DeletingBook()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            string Book_id = Request["Book_id"].ToString();
            Models.BookModel Books = new Models.BookModel();
            int flag = Books.DeleteBook(Book_id);
            if (flag > 0)
            {
                string usreid = Session["auth_userid"].ToString();
                int log_id = Books.AddLog(3, usreid, "Web", "", "");
                TempData["alert"] = "تم مسح الكتاب بنجاح";
            }
            else
                TempData["error"] = "حدث خطأ ، لم يتم مسح الكتاب";
            return RedirectToAction("SearchBook");
        }

        public ActionResult EditBook(string id, string Sh_id, String Sh_BookNum, string ShBookNum, string ShSubject, string ShBook_Date, int? page)
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            
            Models.BookModel Books = new Models.BookModel();
            List<ViewModels.Book_Location> BLocations = new List<ViewModels.Book_Location>();
            List<ViewModels.CopyTo> CopT = new List<ViewModels.CopyTo>();
            List<ViewModels.Attachment> Attach = new List<ViewModels.Attachment>();
            ViewModels.Books BookDetail = new ViewModels.Books();
            ViewBag.alert = TempData["alert"];
            ViewBag.error = TempData["error"];
            ViewBag.CopyTo = 1;
            BookDetail = Books.GetBook(id);

            if (BookDetail != null)
            {
                ViewBag.Books = BookDetail;
                String BookNum;
                if (BookDetail.BookType == "2")
                {
                    
                    int length = BookDetail.Book_Num.Length;
                    if (BookDetail.Book_Num.Contains("113100"))
                    {
                        BookNum = BookDetail.Book_Num.Substring(6, length - 6);
                        BookDetail.Book_Num = BookNum;
                        ViewBag.Book_Num = "113100";
                    }
                    else if (BookDetail.Book_Num.Contains("110100"))
                    {
                        BookNum = BookDetail.Book_Num.Substring(6, length - 6);
                        BookDetail.Book_Num = BookNum;
                        ViewBag.Book_Num = "110100";
                    }
                    else if (BookDetail.Book_Num.Contains("112000"))
                    {
                        BookNum = BookDetail.Book_Num.Substring(6, length - 6);
                        BookDetail.Book_Num = BookNum;
                        ViewBag.Book_Num = "112000";
                    }
                    else if (BookDetail.Book_Num.Contains("114000"))
                    {
                        BookNum = BookDetail.Book_Num.Substring(6, length - 6);
                        BookDetail.Book_Num = BookNum;
                        ViewBag.Book_Num = "114000";
                    }
                    else if (BookDetail.Book_Num.Contains("110000"))
                    {
                        BookNum = BookDetail.Book_Num.Substring(6, length - 6);
                        BookDetail.Book_Num = BookNum;
                        ViewBag.Book_Num = "110000";
                    }
                    else if (BookDetail.Book_Num.Contains("1110100"))
                    {
                        BookNum = BookDetail.Book_Num.Substring(7, length - 7);
                        BookDetail.Book_Num = BookNum;
                        ViewBag.Book_Num = "1110100";
                    }
                    else
                        ViewBag.Book_Num = "";
                }
                else
                {
                    int length = BookDetail.Ward_num.Length;
                    if (BookDetail.Ward_num.Contains("113100"))
                    {
                        BookNum = BookDetail.Ward_num.Substring(6, length - 6);
                        BookDetail.Ward_num = BookNum;
                        ViewBag.Book_Num = "113100";
                    }
                    else if (BookDetail.Ward_num.Contains("110100"))
                    {
                        BookNum = BookDetail.Ward_num.Substring(6, length - 6);
                        BookDetail.Ward_num = BookNum;
                        ViewBag.Book_Num = "110100";
                    }
                    else if (BookDetail.Ward_num.Contains("112000"))
                    {
                        BookNum = BookDetail.Ward_num.Substring(6, length - 6);
                        BookDetail.Ward_num = BookNum;
                        ViewBag.Book_Num = "112000";
                    }
                    else if (BookDetail.Ward_num.Contains("114000"))
                    {
                        BookNum = BookDetail.Ward_num.Substring(6, length - 6);
                        BookDetail.Ward_num = BookNum;
                        ViewBag.Book_Num = "114000";
                    }
                    else if (BookDetail.Ward_num.Contains("110000"))
                    {
                        BookNum = BookDetail.Ward_num.Substring(6, length - 6);
                        BookDetail.Ward_num = BookNum;
                        ViewBag.Book_Num = "110000";
                    }
                    else if (BookDetail.Ward_num.Contains("1110100"))
                    {
                        BookNum = BookDetail.Ward_num.Substring(7, length - 7);
                        BookDetail.Ward_num = BookNum;
                        ViewBag.Book_Num = "1110100";
                    }
                    else
                        ViewBag.Book_Num = "";
                }

                

                BLocations = Books.GetBookLocations(id);
                Attach = Books.GetBookAttachments(id);
                ViewBag.Book_Attach = Attach;
                Session["book_attachments"] = Attach;

                if (BLocations != null)
                {
                    Session["book_Location"] = BLocations;
                    ViewBag.BookLocations = BLocations;
                    if (BookDetail.BookType == "1")
                    {
                        ViewBag.CopyToList = CopT;
                        Session["copy_to"] = CopT;
                        if (BLocations.Count == 0)
                            ViewBag.BookLoc = 1;
                        else
                            ViewBag.BookLoc = BLocations.Count;


                        ViewBag.BookLoc2 = 1;
                        ViewBag.CopyTo = 1;

                    }

                    else
                    {
                        CopT = Books.GetCopyTo(id);
                        ViewBag.CopyToList = CopT;
                        Session["copy_to"] = CopT;

                        ViewBag.BookLoc = 1;
                        if (BLocations.Count == 0)
                            ViewBag.BookLoc2 = 1;
                        else
                            ViewBag.BookLoc2 = BLocations.Count;

                        if (CopT.Count == 0)

                            ViewBag.CopyTo = 1;
                        else
                            ViewBag.CopyTo = CopT.Count;


                    }
                }
            }

            Session["book_id"] = id;
            Session["book_type"] = BookDetail.BookType.ToString();
            Session["resp_date"] = ViewBag.Books.Response_Date.ToString();

            string User_Dept = Session["auth_Department"].ToString();
            if ((Sh_id != null) && (Sh_id != ""))
            {
                ViewBag.Books_Search = Books.PrintBooks("", "", "", "", "",User_Dept).ToPagedList(page ?? 1, 20);
                ViewBag.Books.Parent_id = Sh_id;
                ViewBag.Books.Book_Response = Sh_BookNum;
                ViewBag.JavaScriptFunction = "0";
            }
            else if (((ShBookNum != null) && (ShBookNum != "")) || ((ShSubject != null) && (ShSubject != "")) || ((ShBook_Date != null) && (ShBook_Date != "")))
            {
                ViewBag.JavaScriptFunction = "1";
                ViewBag.Books_Search = Books.PrintBooks(ShBookNum, ShSubject, ShBook_Date, "", "",User_Dept).ToPagedList(page ?? 1, 20);
            }
            else
            {
                ViewBag.Books_Search = Books.PrintBooks("", "", "", "", "",User_Dept).ToPagedList(page ?? 1, 20);
                ViewBag.JavaScriptFunction = "0";
            }
            return View();

        }

        [HttpPost]
        public ActionResult UploadAttach(HttpPostedFileBase Postfile, int? page)
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            List<ViewModels.Book_Location> BLocations = new List<ViewModels.Book_Location>();
            List<ViewModels.CopyTo> CopT = new List<ViewModels.CopyTo>();
            Models.BookModel Books = new Models.BookModel();

            List<ViewModels.Attachment> Attach = new List<ViewModels.Attachment>();
            List<string> NewBookLocation = new List<string>();
            List<string> NewCopyToList = new List<string>();
            ViewModels.Books BookDetail = new ViewModels.Books();

            BookDetail.id = Convert.ToInt32(Request["id2"].ToString());
            BookDetail.Added_Date = Convert.ToDateTime(Request["Added_Date2"].ToString());
            BookDetail.Book_Date = Request["Book_Date2"].ToString();
            BookDetail.Subject = Request["Subject2"].ToString();
            BookDetail.Destination = Request["Destination2"].ToString();
            BookDetail.BookType = Request["BookType2"].ToString();
            BookDetail.Status = Request["Status2"].ToString();
            BookDetail.Duration = Convert.ToInt32(Request["DurationID2"].ToString());
            BookDetail.Book_Response = Request["Book_Response2"].ToString();
            BookDetail.Wazeer_Note = Request["Wazeer_Note2"].ToString();
            BookDetail.Secret = Convert.ToInt32(Request["Secret2"].ToString());
            BookDetail.Response_Date = Request["RespBook_Date2"].ToString();
            BookDetail.Parent_id = Request["Parent_id2"].ToString();
            if(Convert.ToInt16(Request["SaderLocCounter2"].ToString()) == 0)
                ViewBag.BookLoc2 = 1;
            else
                ViewBag.BookLoc2 = Convert.ToInt16(Request["SaderLocCounter2"].ToString());

            if (Convert.ToInt16(Request["CopyCounter2"].ToString()) == 0)
                ViewBag.CopyTo = 1;
            else
                ViewBag.CopyTo = Convert.ToInt16(Request["CopyCounter2"].ToString());

            if (Convert.ToInt16(Request["WardLocCounter2"].ToString()) == 0)
                ViewBag.BookLoc = 1;
            else
                ViewBag.BookLoc = Convert.ToInt16(Request["WardLocCounter2"].ToString());



            int i;
            if (BookDetail.BookType == "1")
            {
                ViewBag.Book_Num = "";
                BookDetail.Book_Num = Request["Book_Num2"].ToString();
                BookDetail.Ward_num = Request["BookWard_Num2"].ToString();
                BookDetail.Ward_Date = Request["BookWard_Date2"].ToString();
                i = 1;
                int Wardcounter = Convert.ToInt16(Request["WardLocCounter2"].ToString());
                while (i <= Wardcounter)
                {
                    string WardLoc = "NWardLoc" + i;
                    //NewBookLocation.Add(Request[WardLoc].ToString());
                    BLocations.Add(new ViewModels.Book_Location
                    {
                        Name = Request[WardLoc].ToString(),
                        Book_type = BookDetail.BookType,
                        Text_id = "WardLoc" + i,
                        Label_text = "مصدر الكتاب " + i
                    });
                    i = i + 1;
                }
            }
            else
            {
                i = 1;
                BookDetail.Book_Num = Request["SaderBook_Num2"].ToString();
                ViewBag.Book_Num = Request["ReadBook_Num2"].ToString();
                int Sadercounter = Convert.ToInt16(Request["SaderLocCounter2"].ToString());
                int CopyCounter = Convert.ToInt16(Request["CopyCounter2"].ToString());
                while (i <= Sadercounter)
                {
                    string SaderLoc = "NewSaderLoc" + i;
                    BLocations.Add(new ViewModels.Book_Location
                    {
                        Name = Request[SaderLoc].ToString(),
                        Book_type = BookDetail.BookType,
                        Text_id = "SaderLoc" + i,
                        Label_text = "الجهة المرسل اليها " + i
                    });
                    i = i + 1;
                }

                i = 1;
                while (i <= CopyCounter)
                {
                    string CopyTo = "NewCopyTo" + i;
                    CopT.Add(new ViewModels.CopyTo
                    {
                        Name = Request[CopyTo].ToString(),
                        Text_id = "CopyTo" + i,
                        Label_text = "نسخة الى " + i
                    });
                    i = i + 1;
                }

                
            }

            string User_Dept = Session["auth_Department"].ToString();
            ViewBag.Books_Search = Books.PrintBooks("", "", "", "", "",User_Dept).ToPagedList(page ?? 1, 20);
            ViewBag.JavaScriptFunction = "0";
            ViewBag.CopyToList = CopT;
            ViewBag.BookLocations = BLocations;
            ViewBag.Books = BookDetail;

            Attach = (List <ViewModels.Attachment>) Session["book_attachments"];
            if ((Postfile != null) && (Postfile.ContentLength > 0))
            {
                string filename = Path.GetFileName(Postfile.FileName);
                string extention = Path.GetExtension(Postfile.FileName).ToLower();
                String[] allowedExtensions = { ".gif", ".png", ".jpeg", ".jpg", ".doc", ".docx", ".pdf", ".fni", ".xls", ".xlt", ".xlm", ".ppt", ".pot", ".pps" };
                if (allowedExtensions.Contains(extention))
                {
                    using (Stream fs = Postfile.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            string contenttype = Postfile.ContentType;
                            int filesize = Postfile.ContentLength;
                            byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            Attach.Add(new ViewModels.Attachment
                            {
                                id = 0,
                                Name = filename,
                                Book_id = BookDetail.id.ToString(),
                                Attach = bytes,
                                content_type = contenttype,
                                size = filesize.ToString()
                            });
                        }
                    }
                }
                else
                    TempData["error"] = "حدث خطأ ، لم يتم إضافة مرفق الكتاب لعدم مطابقته نوع الملف";
            }

            Session["book_attachments"] = Attach;
            ViewBag.Book_Attach = Attach;
            return View("EditBook");
        }

        [HttpPost]
        public ActionResult EditingBook()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            string id = Request["id"].ToString();
            try
            {
                string Book_Num = Request["Book_Num"].ToString();
                string SaderBook_Num = Request["SaderBook_Num"].ToString();
                string Book_Date = Request["Book_Date"].ToString();
                string Subject = Request["Subject"].ToString();
                string Destination = Request["Destination"].ToString();
                string BookType = Request["BookType"].ToString();
                string Status = Request["Status"].ToString();
                string Duration = Request["DurationID"].ToString();
                string Book_Response = Request["Book_Response"].ToString();
                string Wazeer_Note = Request["Wazeer_Noteee"].ToString();
                string Secret = Request["Secret"].ToString();
                string Response_Date = Request["RespBook_Date"].ToString();
                string Parent_id = Request["Parent_id"].ToString();
                string Old_BookType = (string)Session["book_type"];
                int Sadercounter = Convert.ToInt16(Request["SaderLocCounter"].ToString());
                int CopyCounter = Convert.ToInt16(Request["CopyCounter"].ToString());
                int Wardcounter = Convert.ToInt16(Request["WardLocCounter"].ToString());
                List<string> NewBookLocation = new List<string>();
                List<string> NewCopyToList = new List<string>();
                DateTime Added_Date = Convert.ToDateTime(Request["Added_Date"].ToString());
                DateTime B_Date, W_Date;
                DateTime? R_Date;
                if ((Session["resp_date"].ToString() != null) && (Session["resp_date"].ToString() != ""))
                    R_Date = Convert.ToDateTime(Session["resp_date"]);
                else
                    R_Date = null;

                    
                string WardBook_Num = "";
                string WardBook_Date;
                int WardFlag = 1;
                W_Date = DateTime.Now;

                List<ViewModels.Book_Location> BLocations = new List<ViewModels.Book_Location>();
                List<ViewModels.CopyTo> CopT = new List<ViewModels.CopyTo>();
                List<ViewModels.Attachment> Attachs = new List<ViewModels.Attachment>();
                Attachs = (List<ViewModels.Attachment>)Session["book_attachments"];
                BLocations = (List<ViewModels.Book_Location>)Session["book_Location"];
                CopT = (List<ViewModels.CopyTo>)Session["copy_to"];

                if (BookType == "1")
                {
                    WardBook_Num = Request["BookWard_Num"].ToString();
                    WardBook_Date = Request["BookWard_Date"].ToString();
                    if (DateTime.TryParse(WardBook_Date, out W_Date) == false)
                        WardFlag = 0;
                    else
                    {
                        int i = 1;
                        while (i <= Wardcounter)
                        {
                            string WardLoc = "WardLoc" + i;
                            NewBookLocation.Add(Request[WardLoc].ToString());
                            i = i + 1;
                        }
                    }
                }
                else
                {
                    int i = 1;
                    while (i <= Sadercounter)
                    {
                        string SaderLoc = "SaderLoc" + i;
                        NewBookLocation.Add(Request[SaderLoc].ToString());
                        i = i + 1;
                    }

                    i = 1;
                    while (i <= CopyCounter)
                    {
                        string CopyTo = "CopyTo" + i;
                        NewCopyToList.Add(Request[CopyTo].ToString());
                        i = i + 1;
                    }
                }

                if ((WardFlag == 0) || (DateTime.TryParse(Book_Date, out B_Date) == false) || ((BookType == "1") && (Book_Num.ToString().Count() == 0)) || ((BookType == "2") && (SaderBook_Num.ToString().Count() == 0)) || (Book_Date.ToString().Count() == 0) || (Subject.ToString().Count() == 0))
                {
                    TempData["error"] = "حدث خطأ ، لم يتم إضافة الكتاب ... الرجاء ادخال البيانات الكتاب المطلوبة";
                }
                else
                {
                    Models.BookModel Books = new Models.BookModel();
                    string Attach_id = "0";
                    DateTime? Resp_Date;
                    if ((Response_Date != null) && (Response_Date != ""))
                        Resp_Date = Convert.ToDateTime(Response_Date);
                    else
                        Resp_Date = null;


                    int durat = 1;
                    foreach (var att in Attachs)
                    {
                        if ((att.Attach != null) && (Convert.ToInt32(att.size) > 0))
                            Attach_id = Books.AddOrUdateFile(att.Attach, att.Name, att.content_type, Convert.ToInt32(att.size), id, att.id);
                    }

                    if ((Status.ToString() == "2") && (Duration != "0"))
                    {

                        DateTime R_Date1 = new DateTime();
                       R_Date1 = Added_Date;
                        while (durat <= Convert.ToInt16(Duration))
                        {
                            R_Date1 = R_Date1.AddDays(1);
                            if ((R_Date1.DayOfWeek.ToString() != "Friday") && (R_Date1.DayOfWeek.ToString() != "Saturday"))
                                durat += 1;
                        }
                        Resp_Date = R_Date1;
                    }
                    else if (Status.ToString() == "1")
                        Duration = "0";

                    int flag;
                    if (BookType == "1")
                    {
                        WardBook_Num = Request["ReadBook_Num2"].ToString() + Request["BookWard_Num"].ToString();
                        flag = Books.EditBook(id, Book_Num, B_Date, Subject, BookType, Destination, Status, Book_Response, Duration, Wazeer_Note, Secret, Attach_id, Resp_Date, Parent_id, WardBook_Num, W_Date);
                    }
                    else
                    {
                        SaderBook_Num = Request["ReadBook_Num"].ToString() + Request["SaderBook_Num"].ToString();
                        flag = Books.EditBook(id, SaderBook_Num, B_Date, Subject, BookType, Destination, Status, Book_Response, Duration, Wazeer_Note, Secret, Attach_id, Resp_Date, Parent_id, "", null);
                    }
                        

                    if (Old_BookType != BookType)
                    {
                        int DeleteFlag;
                        DeleteFlag = Books.DeleteBookLoc(id);
                        int BookLocFlag = Books.AddLocation(NewBookLocation,id,BookType);
                        if (BookType == "1")
                            Books.DeleteCopyTo(id);
                        else
                        {
                            int CopyFlag = Books.AddCopyTo(NewCopyToList, id);
                        }

                    }
                    else
                    {
                        int EditBLoc = Books.EditBookLocation(BLocations, NewBookLocation, BookType, id);
                        int EditCopyTo = Books.EditCopyTo(CopT, NewCopyToList, id);
                        
                    }

                    if (flag > 0)
                    {
                        TempData["alert"] = "تم التعديل على الكتاب بنجاح";
                        string usreid = Session["auth_userid"].ToString();
                        int log_id = Books.AddLog(2, usreid, "Web", "", "");
                        return RedirectToAction("SearchBook");
                    }
                    else
                        TempData["error"] = "حدث خطأ ، لم يتم التعديل على الكتاب ";
                }
            }
            catch(Exception ex)
            {
                throw ex;
                TempData["error"] = "حدث خطأ ، لم يتم التعديل على الكتاب";
            }

            return RedirectToAction("EditBook" , new { id = id });
        }


        public ActionResult ViewDetails(string id)
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            Models.BookModel Books = new Models.BookModel();
            List<ViewModels.Attachment> Attach = new List<ViewModels.Attachment>();

            Attach = Books.GetBookAttachments(id);
            ViewBag.Book_Attach = Attach;
            ViewBag.Books = Books.GetBook(id);
            ViewBag.ParentBooks = Books.PrintAllPaernt(id);
            return View();
        }



        public ActionResult NotAnsweredBook(string Book_num, string Subject, string Book_Date, string Booktype, int? page)
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            Models.BookModel Books = new Models.BookModel();
            string User_Dept = Session["auth_Department"].ToString();
            ViewBag.Books = Books.PrintBooks(Book_num, Subject, Book_Date, "2", Booktype,User_Dept).ToPagedList(page ?? 1, 20);
            ViewBag.BType = Booktype;
            DateTime TodayD, Response;
            TimeSpan SubTime;
            foreach (var Book in ViewBag.Books)
            {
                TodayD = DateTime.Now;
                Response = Convert.ToDateTime(Book.Book_ResponseDate);
                SubTime = Response.Subtract(TodayD);
                if (Convert.ToInt16(SubTime.Days) == 0)
                {
                    if ((Convert.ToInt16(SubTime.Hours) > 0) || (Convert.ToInt16(SubTime.Minutes) > 0))
                        Book.Book_rem = 1;
                    else
                        Book.Book_rem = 0;
                }
                else
                    Book.Book_rem = Convert.ToInt16(SubTime.Days);
            }
            return View(ViewBag.Books);
        }


        [HttpPost]
        public ActionResult NotAnsweredActionBook()
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            string Book_num = Request["Book_num"].ToString();
            string Subject = Request["Subject"].ToString();
            string Book_Date = Request["Book_Date"].ToString();
            string Booktype = "";
            return RedirectToAction("NotAnsweredBook", new { Book_num, Subject, Book_Date , Booktype });
        }



        [HttpPost]
        public ActionResult ProcessForm(string Book_num, string Subject, string Book_Date, string Booktype, int? page, string All, string Send, string Recived)
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            Models.BookModel Books = new Models.BookModel();
            string User_Dept = Session["auth_Department"].ToString();

            if (!string.IsNullOrEmpty(All))
            {

                ViewBag.Books = Books.PrintBooks(Book_num, Subject, Book_Date, "2", "",User_Dept).ToPagedList(page ?? 1, 20);
                Booktype = "";

            }

            if (!string.IsNullOrEmpty(Send))
            {
                
                ViewBag.Books = Books.PrintBooks(Book_num, Subject, Book_Date, "2", "1",User_Dept).ToPagedList(page ?? 1, 20);
                Booktype = "1";
                
            }
            if (!string.IsNullOrEmpty(Recived))
            {
                ViewBag.Books = Books.PrintBooks(Book_num, Subject, Book_Date, "2", "2",User_Dept).ToPagedList(page ?? 1, 20);
                Booktype = "2";
            }

            DateTime TodayD, Response;
            TimeSpan SubTime;
            foreach (var Book in ViewBag.Books)
            {
                TodayD = DateTime.Now;
                Response = Convert.ToDateTime(Book.Book_ResponseDate);
                SubTime = Response.Subtract(TodayD);
                if (Convert.ToInt16(SubTime.Days) == 0)
                {
                    if ((Convert.ToInt16(SubTime.Hours) > 0) || (Convert.ToInt16(SubTime.Minutes) > 0))
                        Book.Book_rem = 1;
                    else
                        Book.Book_rem = 0;
                }
                else
                    Book.Book_rem = Convert.ToInt16(SubTime.Days);
            }

            return RedirectToAction("NotAnsweredBook", new { Book_num, Subject, Book_Date, Booktype });
        }

        [HttpGet]
        public ActionResult GetFile(String id)
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            id = Server.HtmlEncode(id);
            Models.BookModel Books = new Models.BookModel();
            ViewModels.Attachment file = Books.DownloadFile(id);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if((file.content_type == "doc") || (file.content_type == "docx"))
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + file.Name);
            return File(file.Attach, file.content_type);

        }


        [HttpGet]
        public ActionResult GetBookID(String id, string BNum)
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            string book_id = (string)Session["book_id"];
            return RedirectToAction("EditBook", new { id = book_id, Sh_id = id , Sh_BookNum = BNum });

        }

        [HttpPost]
        public ActionResult GetSearchBooks(string id, int? page)
        {
            if (Session["auth_isLogin"] == null || (bool)Session["auth_isLogin"] == false)
            {
                return RedirectToAction("Login", "Users");

            }
            string Book_num = Request["ShBook_num"].ToString();
            string Subject = Request["ShSubject"].ToString();
            string Book_Date = Request["ShBook_Date"].ToString();
            Models.BookModel Books = new Models.BookModel();
            ViewBag.JavaScriptFunction = string.Format("Search_buttonClicked('{0}');", 1);
            return RedirectToAction("EditBook", new { id = id , Mod_id = "1" , ShBookNum = Book_num , ShSubject = Subject , ShBook_Date = Book_Date });
        }
    }
}