﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace correspondence.Controllers
{
    public class ApiController : Controller
    {
        [HttpPost]
        public ActionResult Login()
        {
            try
            {
                Stream req = Request.InputStream;
                req.Seek(0, SeekOrigin.Begin);
                string json = new StreamReader(req).ReadToEnd();
                var body = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                Models.UserModel model = new Models.UserModel();
                ViewModels.UserVM user = model.Login(body["username"], body["password"],2);
                if (user != null)
                    return Content(JsonConvert.SerializeObject(user), "application/json");
                else
                    return Content("{\"success\": true ,\"auth\": false , \"message\": \" اسم النستخدم او كلمة المرور خاطئة\"}", "application/json"); ;

            }
            catch (Exception ex)
            {
                return Content("{\"success\": false ,\"auth\": false , \"message\": \"" + ex.ToString() + "\"}", "application/json");
            }
        }



        [HttpPost]
        public ActionResult getBooks()
        {
            try
            {
                Stream req = Request.InputStream;
                req.Seek(0, SeekOrigin.Begin);
                string json = new StreamReader(req).ReadToEnd();
                var body = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                Models.UserModel model = new Models.UserModel();
                
                bool isAuth = model.isAuth(body["username"], body["token"]);
                if (isAuth)
                {
                    Models.BookModel bookModel = new Models.BookModel();
                    var books = bookModel.PrintBooks("", "", "", "", "","");
                    return Content(JsonConvert.SerializeObject(books), "application/json");
                }
                    
                else
                    return Content("{\"success\": true ,\"auth\": false , \"message\": \" بيانات الدخول خاطئة\"}", "application/json"); ;

            }
            catch (Exception ex)
            {
                return Content("{\"success\": false ,\"auth\": false , \"message\": \"" + ex.ToString() + "\"}", "application/json");
            }
        }



        [HttpPost]
        public ActionResult getBook()
        {
            try
            {
                Stream req = Request.InputStream;
                req.Seek(0, SeekOrigin.Begin);
                string json = new StreamReader(req).ReadToEnd();
                var body = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                Models.UserModel model = new Models.UserModel();

                bool isAuth = model.isAuth(body["username"], body["token"]);
                if (isAuth)
                {
                    Models.BookModel bookModel = new Models.BookModel();
                    var book = bookModel.GetBook(body["id"]);
                    return Content(JsonConvert.SerializeObject(book), "application/json");
                }

                else
                    return Content("{\"success\": true ,\"auth\": false , \"message\": \" بيانات الدخول خاطئة\"}", "application/json"); ;

            }
            catch (Exception ex)
            {
                return Content("{\"success\": false ,\"auth\": false , \"message\": \"" + ex.ToString() + "\"}", "application/json");
            }
        }
    }
}